import React from "react";
import { Row, Col, Image, Table } from "react-bootstrap";
import Card from "../../../components/Card";
import { Link } from "react-router-dom";

//progressbar
import Progress from "../../../components/progress.js";

// img
import shap1 from "../../../assets/images/shapes/01.png";
import shap2 from "../../../assets/images/shapes/02.png";
import shap3 from "../../../assets/images/shapes/03.png";
import shap4 from "../../../assets/images/shapes/04.png";
import shap5 from "../../../assets/images/shapes/05.png";
import shap6 from "../../../assets/images/shapes/06.png";

const BootstrapTable = () => {
  return (
    <>
      <Row>
        <Col sm="12">
          <Card>
            <Card.Header className="d-flex justify-content-between">
              <div className="header-title">
                <h4 className="card-title">Suivi demande de remboursement</h4>
              </div>
            </Card.Header>
            <Card.Body className="p-0">
              <div className="table-responsive mt-4">
                <Table striped id="basic-table" className=" mb-0" role="grid">
                  <thead>
                    <tr>
                      <th>Nom</th>
                      <th>Statut</th>
                      <th>Completion</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                     
                      <td>Médicaments</td>
                      <td>
                        <div className="text-info">Pending</div>
                      </td>
                      <td>
                        <div className="d-flex align-items-center mb-2">
                          <h6>60%</h6>
                        </div>
                        <Progress
                          softcolors="info"
                          color="info"
                          className="shadow-none w-100 "
                          value={60}
                          minvalue={0}
                          maxvalue={100}
                          style={{ height: "6px" }}
                        />
                      </td>
                      <td>en cours</td>
                    </tr>
                    <tr>
                      <td>Examens médicaux</td>
                      <td>
                        <div className="text-danger">Pending</div>
                      </td>
                      <td>
                        <div className="d-flex align-items-center mb-2">
                          <h6>10%</h6>
                        </div>
                        <Progress
                          softcolors="danger"
                          color="danger"
                          className="shadow-none w-100"
                          value={10}
                          minvalue={0}
                          maxvalue={100}
                          style={{ height: "6px" }}
                        />
                      </td>
                      <td>en cours</td>
                    </tr>
                    <tr>
                      <td>Hospitalisation </td>
                      <td>
                        <div className="text-success">Completed</div>
                      </td>
                      <td>
                        <div className="d-flex align-items-center mb-2">
                          <h6>100%</h6>
                        </div>
                        <Progress
                          softcolors="success"
                          color="success"
                          className="shadow-none w-100"
                          value={100}
                          minvalue={0}
                          maxvalue={100}
                          style={{ height: "6px" }}
                        />
                      </td>
                      <td>en cours</td>
                    </tr>
                  </tbody>
                </Table>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </>
  );
};

export default BootstrapTable;
