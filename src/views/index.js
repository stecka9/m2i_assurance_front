import React, { useState, useEffect, Fragment } from 'react';
import { Link } from 'react-router-dom';
import topImage from '../assets/images/hh.png';

const Index = () => {
return (
<Fragment>
<span className="uisheet screen-darken"></span>
<div
className="header"
style={{
  background: `url(${topImage})`,
  backgroundSize: "cover",
  backgroundRepeat: "no-repeat",
  height: "100vh",
  position: "relative",
}}

>
<div className="main-img">
<div className="container">
<div className="row justify-content-center align-items-center">
<div className="col-md-8 col-lg-6">
<h1 className="text-white text-center mb-5">
Bienvenue sur notre assurance maladie
</h1>
<p className="text-white text-center mb-5">
Nous sommes là pour vous accompagner dans vos démarches de remboursement de frais de santé.
</p>
<div className="d-flex justify-content-center">
<Link
                 className="btn btn-light bg-white d-flex"
                 to="/dashboard"
                 target="_blank"
               >
Accéder à mon espace personnel
</Link>
</div>
</div>
</div>
</div>
</div>
</div>
</Fragment>
);
};

export default Index;





