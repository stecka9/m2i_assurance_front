FROM node:17-alpine
WORKDIR /app
COPY package*.json .
RUN npm install
COPY . .
EXPOSE 9010
CMD [ "npm","start" ]